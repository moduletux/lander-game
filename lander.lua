-- title:  Lander
-- author: David Hollinger III
-- desc:   Simple lander game
-- script: lua

--------------
--INITIALIZE--
--------------

-- Initialization
 function init()
  gndclr = math.random(2,15)
  game_over = false
  g = 0.020 --gravity
  make_player()
  make_ground()
  make_sparks()
  if (not lvlup) then
    score = 0
    newgame = true
  end
  lvlup = false
  fuel = 500
  music(0,-1,-1,false)
end

-- player creation
function make_player()
  p={}
  p.x=60 --position
  p.y=8
  p.dx=0 --movement
  p.dy=0
  p.sprite=1
  p.alive=true
  p.thrust=0.050
end

--ground creation
function make_ground()
  --create the ground
  gnd={}
  local top=96 --highest point
  local btm=129 --lowest point

  --set up the landing pad
  pad={}
  pad.width=15
  pad.x=math.random(0,239-pad.width)
  pad.y=math.random(top,btm)
  pad.sprite=2

  --create ground at pad
  for i=pad.x,pad.x+pad.width do
    gnd[i]=pad.y
  end

  --create ground right of pad
  for i=pad.x+pad.width+1,239 do
    local h=math.random(gnd[i-1]-3,gnd[i-1]+3)
    gnd[i]=mid(top,h,btm)
  end

  --create ground left of pad
  for i=pad.x-1,0,-1 do
    local h=math.random(gnd[i+1]-3,gnd[i+1]+3)
    gnd[i]=mid(top,h,btm)
  end
end

--make sparks
function make_sparks()
  sparks={}

  for i=1,200 do
    sparks[i] = {
      x=0,
      y=0,
      velx=0,
      vely=0,
      r=0,
      alive=false,
      mass=0
    }
  end
end

--pico-8 mid function
function mid(a,b,c)
  if a<=b and a<=c then return math.max(a,math.min(b,c))
  elseif b<=a and b<=c then return math.max(b,math.min(a,c)) end
  return math.max(c,math.min(a,b))
end

--call main
init()

--game loop
function TIC()
  update()
  draw()
end

---------
--INPUT--
---------

--move player
function move_player()
  p.dy = p.dy + g --add gravity

  thrust()

  p.x = p.x + p.dx -- actually move
  p.y = p.y + p.dy -- the player

  stay_on_screen()
end

--calculate thrust
function thrust()
  --add thrust to movement
  if (btn(2)) then
    p.dx = p.dx - p.thrust
  end
  if (btn(3)) then
    p.dx = p.dx + p.thrust
  end
  if (btn(0)) then
    p.dy = p.dy - p.thrust
  end

  --thrust sound
  if (btn(0) or btn(1) or btn(2)) then
    sfx(0)
  end
end

-------------------------
--UPDATE GAME FUNCTIONS--
-------------------------

--game loop update
function update()
  if newgame then
    titleupdate()
  else
    gameupdate()
  end
end

--update title data
function titleupdate()
  if btnp(4) then
    newgame = false
    gameupdate()
  end
end

--update game
function gameupdate()
  music()

  for i=1,#sparks do
    if sparks[i].alive then
      sparks[i].x = sparks[i].x + (sparks[i].velx/sparks[i].mass)
      sparks[i].y = sparks[i].y + (sparks[i].vely/sparks[i].mass)
      sparks[i].r = sparks[i].r - 0.1

      if sparks[i].r < 0.1 then
        sparks[i].alive = false
      end
    end
  end

  if (game_over) then
    if (btnp(5)) then
      init()
    end
  elseif (lvlup) then
    if (btnp(4)) then
      init()
    elseif (btnp(5)) then
      lvlup = false
      init()
    end
  else
    move_player()
    check_land()
    calculate_fuel()
  end
end

------------------
--UPDATE HELPERS--
------------------

--edge of screen collision
function stay_on_screen()
  if (p.x < 0) then --left side
    p.x = 0
    p.dx = 0
  end

  if (p.x > 232) then
    p.x = 232
    p.dx = 0
  end

  if (p.y < 0) then
    p.y = 0
    p.dy = 0
  end
end

--check if landed or crashed
function check_land()
  l_x=math.floor(p.x) --left side of ship
  r_x=math.floor(p.x+7) --right side of ship
  b_y=math.floor(p.y+7) --bottom of ship

  over_pad = l_x >= pad.x and r_x <= pad.x + pad.width
  on_pad = b_y >= pad.y - 1
  slow = p.dy < 1

  if (over_pad and on_pad and slow) then
    next_level()
  elseif ((over_pad and on_pad) or (fuel == 0)) then
    end_game()
  else
    for i=l_x,r_x do
      if (gnd[i] <= b_y) then
        end_game()
      end
    end
  end
end

--game over function
function end_game()
  explode(p.x,p.y,5,5+math.random(5))
  game_over=true
  score = score + fuel
  sfx(2)
end

--next level
function next_level()
  lvlup=true
  score = score + fuel
  sfx(1)
end

--trigger explosion
function explode(x,y,r,particles)
  local selected=0
  for i=1,#sparks do
    if not sparks[i].alive then
      sparks[i].x = x
      sparks[i].y = y
      sparks[i].vely = -1 + math.random(0,2)
      sparks[i].velx = -1 + math.random(0,2)
      sparks[i].mass = 0.5 + math.random(2)
      sparks[i].r = 0.5 + math.random(r)
      sparks[i].alive = true

      selected = selected + 1
      if selected == particles then
        break
      end
    end
  end
end

--fuel calculation
function calculate_fuel()
  fuel = fuel - 1
end

------------------------
-- DRAW GAME FUNCTIONS--
------------------------

--draw screen
function draw()
  if newgame then
    titledraw()
  else
    gamedraw()
  end
end

--draw title on screen
function titledraw()
  cls()
  draw_stars()
  spr(17,57,50,-1,2,0,0,8,2)
  text="press A to play"
  print(text,hcenter(text),90,15,false,1,false)
end

--draw actual game
function gamedraw()
  cls()
  draw_stars()
  draw_ground()
  draw_player()
  draw_sparks()
  print("fuel: "..fuel,0,0,7)

  if (lvlup) then
    gotxt = "Level completed!"
    print(gotxt,hcenter(gotxt),48,11)
    scprt="Score: "
    text="Press A to play next level"
    end_print()
  elseif (game_over) then
    if (fuel < 1) then
      gotxt = "You ran out of fuel"
    else
      gotxt = "You crashed!"
    end
    scprt = "Final Score: "
    print(gotxt,hcenter(gotxt),48,6)
    end_print()
  end
end

--draw the ground and landing pad
function draw_ground()
  for i=0,239 do
    line(i,gnd[i],i,135,gndclr)
  end
  spr(pad.sprite,pad.x,pad.y,0,1,0,0,2,1)
end

--draw player ship
function draw_player()
  if (not game_over and lvlup) then
    spr(p.sprite,p.x,p.y)
    spr(4,p.x,p.y-8)
  elseif (not game_over) then
    spr(p.sprite,p.x,p.y)
  end
end

--draw explosion effects
function draw_sparks()
  for i=1,#sparks do
    if sparks[i].alive then
      circ(
        sparks[i].x,
        sparks[i].y,
        sparks[i].r,
        5+math.random(10)
      )
    end
  end
end

--draw background stars
function draw_stars()
  math.randomseed(2)
  for i=1,100 do
    pix(math.random(0,239),math.random(0,135),15)
  end
  math.randomseed(time())
end

-----------
--HELPERS--
-----------

--end of level output
function end_print()
  scoretxt=scprt..score
  print(scoretxt,hcenter(scoretxt),57,57,15)
  if (lvlup) then
    print(text,hcenter(text),70,5)
  end
  text2="Press B to return to title"
  print(text2,hcenter(text2),80,5)
end

-- Center text
function hcenter(s)
  local width=print(s,0,-6)
  return (240-width)/2
end

-- <TILES>
-- 001:0007700000077000007df700077dd770777dd77777777777077777700a0aa0a0
-- 002:fa133333faaaaaaa00faaaaa0000000000000000000000000000000000000000
-- 003:33333faaaaaaaaaaaaaaaa000000000000000000000000000000000000000000
-- 004:000000000000000000000000000ba00000bba0000bbba0000000a0000000a000
-- 017:0000000009900000099000000990000009900000099000000990000009900000
-- 018:0000000000000099000000990000993300009900000099000000990000009900
-- 019:0000000099000990990009993399099900990999009909930099099000990990
-- 020:0000000000000990900009909000099099900990399009900999999003399990
-- 021:0000000009999999099333330990000009900000099000000990000009900000
-- 022:0000000099009999399099330990990009909900099099000990990009909900
-- 023:0000000099909999333099000000990000009900000099000000990000009900
-- 024:0000000099900000339900000099000000990000009900000099000000990000
-- 033:0990000009900000099000000990000009900000099000000999999903333333
-- 034:0000999900009933000099000000990000009900000099009990990033303300
-- 035:9999099033990990009909900099099000990990009909900099099000330330
-- 036:0009999000033990000009900000099000000990000009900000099000000330
-- 037:0990000009900000099000000990000009900000099000000999999903333333
-- 038:0990999909909933099099000990990009909900099099009930999933003333
-- 039:9900999933009933000099000000990000009900000099009990990033303300
-- 040:9933000033000000009900000099000000990000009900000099000000330000
-- </TILES>

-- <WAVES>
-- 000:00000000ffffffff00000000ffffffff
-- 001:0123456789abcdeffedcba9876543210
-- 002:0123456789abcdef0123456789abcdef
-- </WAVES>

-- <SFX>
-- 000:83f183f4f303f300f30cf309f308f308f30bf307f307f304f30bf30bf30cf300f304f304f304f304f30cf30bf30bf305f307f308f308f309f30ff30f233000000000
-- 001:00e000e090e090e000b000b090b090b0008000809080908000d000d090d090d0f000f0000080008000f000f090f090f0f000f000f000f000f000f000165000000000
-- 002:0300030023002300430043006300630063006300830083008300830083008300a300a300a300a300a300a300a300a300c300c300c300c300f300f300401000000000
-- 003:00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010a000000000
-- </SFX>

-- <PATTERNS>
-- 000:800036600036700036700036700036600036700036700036600036700036700036400036400036400036400036400036100030100030100030000000000000100030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
-- </PATTERNS>

-- <TRACKS>
-- 000:100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ced210
-- </TRACKS>

-- <PALETTE>
-- 000:0000001d2b537e255383769cab5236008751ff004d5f574fff77a8ffa300c2c3c700e436ffccaa29adffffec27fff1e8
-- </PALETTE>

